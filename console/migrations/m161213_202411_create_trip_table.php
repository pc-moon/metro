<?php

use yii\db\Migration;

/**
 * Handles the creation of table `trip`.
 */
class m161213_202411_create_trip_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('trip', [
            'id' => $this->primaryKey(),

            'cost' => $this->integer(),
            'trip_from' =>  $this->string(),
            'trip_to' =>  $this->string(),
            'user_id' => $this->integer()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('trip');
    }
}
