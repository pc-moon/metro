<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ticket`.
 */
class m161213_202440_create_ticket_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('ticket', [
            'id' => $this->primaryKey(),

            'passenger_id' => $this->integer(),
            'trip_id' => $this->integer(),
            'vip'=> $this->integer(),

            'user_id' => $this->integer()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('ticket');
    }
}
