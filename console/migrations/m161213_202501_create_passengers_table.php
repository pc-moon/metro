<?php

use yii\db\Migration;

/**
 * Handles the creation of table `passengers`.
 */
class m161213_202501_create_passengers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('passengers', [
            'id' => $this->primaryKey(),
            'fullname' => $this->string(),
            'email' => $this->string(),
            'idno' => $this->integer(13),
            'mobile' => $this->integer(13),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('passengers');
    }
}
