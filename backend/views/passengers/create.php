<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Passengers */

$this->title = Yii::t('app', 'Create Passengers');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Passengers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="passengers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
