<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "trip".
 *
 * @property integer $id
 * @property integer $cost
 * @property string $trip_from
 * @property string $trip_to
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class Trip extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trip';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cost', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'required'],
            [['trip_from', 'trip_to'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'cost' => Yii::t('app', 'Cost'),
            'trip_from' => Yii::t('app', 'Trip From'),
            'trip_to' => Yii::t('app', 'Trip To'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return TripQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TripQuery(get_called_class());
    }
}
