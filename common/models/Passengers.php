<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "passengers".
 *
 * @property integer $id
 * @property string $fullname
 * @property string $email
 * @property integer $idno
 * @property integer $mobile
 * @property integer $created_at
 * @property integer $updated_at
 */
class Passengers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'passengers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idno', 'mobile', 'created_at', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'required'],
            [['fullname', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fullname' => Yii::t('app', 'Fullname'),
            'email' => Yii::t('app', 'Email'),
            'idno' => Yii::t('app', 'Idno'),
            'mobile' => Yii::t('app', 'Mobile'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return PassengersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PassengersQuery(get_called_class());
    }
}
