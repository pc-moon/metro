<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Passengers]].
 *
 * @see Passengers
 */
class PassengersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Passengers[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Passengers|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
