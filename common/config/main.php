<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [

        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'common\models\User',
        ],

        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
];
